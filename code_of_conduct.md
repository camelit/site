---
layout: page 
title: Code of Conduct 
description: Please be nice to each other, <br> so we can all enjoy the event.
image: socrates_crete_oac_view.jpeg
---

## Inclusiveness

We welcome and support people of all backgrounds and identities. This includes, but is not limited to, people of any sexual orientation, gender identity and expression, race, ethnicity, culture, national origin, social and economic class, educational level, colour, immigration status, sex, age, size, family status, political belief, religion, and mental and physical ability.

## Be respectful

Please show respect to everyone in and around the Event Venue. This is not exclusively for people attending the Event but also members of the public and venue Staff who may be in and around the venue. Not all of us will agree with each other all the time, but disagreement is no excuse for disrespectful behaviour and manners. We might all experience some frustration now and then, but we cannot allow that frustration to turn into a personal attack. An environment where people feel uncomfortable or threatened is not a productive or creative one.

## Be aware of the effect your words may have on others

We are a community of professionals, and we conduct ourselves professionally. Be kind to others. Do not insult or put down other participants. Harassment and other exclusionary behaviour aren’t acceptable. In general, if someone asks you to stop something, then stop.

This includes, but is not limited to:

- Threats of violence
- Publishing or telling others, that a participant belongs to a particular identity channel without asking their consent first.
- Discriminatory jokes and language
- Sharing sexually explicit or violent material via electronic devices or other means
- Insulting or putting down other participants.
- Aggressive or sexualised language and content. Unwanted sexual advances.
- Advocating for, or encouraging, any of the above behaviour
- “Well-actuallies”: Telling people what they “actually meant to say”.

## Application

This code of conduct applies to the SoCraTes Crete event itself, and to all digital spaces that are related to the SoCraTes Crete unconference, such as our discord server and our social networks.

## CoC Violations

If you think someone has violated our code of conduct —even if you were not directly involved, like you just overheard a conversation— please:

- Let the person know that what they did is not appropriate and ask them to stop.
- Contact the organisers of SoCraTes Crete:
  - Through our discord (See `@OrgTeam` members))
  - Through a direct message to [our mastodon profile]({{site.mastodon_url}}))
  - Through a direct message to [our twitter profile]({{site.twitter_url}}))
  - Through our email [{{site.email}}](mailto:{{site.email}})
  - Contact in person (especially during the event)

But please give people the benefit of doubt. If there is even a slight chance that this was a misunderstanding (e.g. the person did not speak in their native language, and did not find the right words), try to sort it out in a friendly, constructive way. When we learn about a CoC violations, organisers will hear both sides, and then take action we deem appropriate, such as:

- Give a warning
- Have a longer talk about our values
- Expel the perpetrator from the conference without a refund (requires two organisers to agree, if this was the first offence)
- Call the authorities


