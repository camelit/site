---
full_name: Vagelis Antoniadis
image: vanton.jpg
website: https://about.me/vantoniadis
linkedin: http://www.linkedin.com/in/vanton
twitter: https://www.twitter.com/vanton_
mastodon: https://mastodon.social/web/@vanton
github: https://github.com/vanton1
gitlab: https://gitlab.com/vanton
---
